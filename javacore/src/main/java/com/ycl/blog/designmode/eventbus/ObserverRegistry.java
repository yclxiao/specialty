package com.ycl.blog.designmode.eventbus;

import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * User: 杨成龙
 * Date: 2020/4/13
 * Time: 9:16 上午
 * Desc: 注册观察者
 */
public class ObserverRegistry {

    private ConcurrentHashMap<Class<?>, CopyOnWriteArraySet<ObserverAction>> registry = new ConcurrentHashMap<>();

    /**
     * 注册
     */
    public void register(Object observer) {
        //遍历带有注解的方法，将事件和对应的多个处理方法，存储到map中
        Map<Class<?>, Collection<ObserverAction>> observerActions = findAllObserverActions(observer);

        //将获取到的单个观察者的可执行方法，放到如全局的map中，使用并发类
        for (Map.Entry<Class<?>, Collection<ObserverAction>> entry : observerActions.entrySet()) {
            Class<?> eventType = entry.getKey();
            Collection<ObserverAction> eventActions = entry.getValue();
            CopyOnWriteArraySet<ObserverAction> registeredEventActions = registry.get(eventType);
            if (registeredEventActions == null) {
                registry.putIfAbsent(eventType, new CopyOnWriteArraySet<>());
                registeredEventActions = registry.get(eventType);
            }
            registeredEventActions.addAll(eventActions);
        }
    }

    /**
     * 遍历带有注解的方法，将事件和对应的多个处理方法，存储到map中
     *
     * @param observer
     * @return
     */
    private Map<Class<?>, Collection<ObserverAction>> findAllObserverActions(Object observer) {
        Class<?> clazz = observer.getClass();
        List<Method> methodList = getAnnotateMethods(clazz);

        Map<Class<?>, Collection<ObserverAction>> observerActions = new HashMap<>();
        for (Method method : methodList) {
            Class<?>[] parameterTypes = method.getParameterTypes();
            Class<?> eventType = parameterTypes[0];
            if (!observerActions.containsKey(eventType)) {
                observerActions.put(eventType, new ArrayList<>());
            }
            observerActions.get(eventType).add(new ObserverAction(observer, method));
        }
        return observerActions;
    }

    /**
     * 获取观察者中含有注解的方法
     *
     * @param clazz
     * @return
     */
    private List<Method> getAnnotateMethods(Class<?> clazz) {
        List<Method> annotateMethods = new ArrayList<>();
        for (Method method : clazz.getDeclaredMethods()) {
            if (method.isAnnotationPresent(Subscribe.class)) {
//                Class<?>[] parameterTypes = method.getParameterTypes();
                annotateMethods.add(method);
            }
        }

        return annotateMethods;
    }

    /**
     * 根据事件获取合适的观察者方法
     *
     * @param event
     * @return
     */
    public List<ObserverAction> getMatchedObserverActions(Object event) {
        List<ObserverAction> matchedObservers = new ArrayList<>();
        Class<?> postedEventType = event.getClass();
        for (Map.Entry<Class<?>, CopyOnWriteArraySet<ObserverAction>> entry : registry.entrySet()) {
            Class<?> eventType = entry.getKey();
            Collection<ObserverAction> eventActions = entry.getValue();
            //判断有入参的事件，是否是容器里的时间的子类，可以说是一个类是否可以被强制转换为另外一个实例对象
            //父类 和 子类，判断都会为true
            if (eventType.isAssignableFrom(postedEventType)) {
                matchedObservers.addAll(eventActions);
            }
        }
        return matchedObservers;
    }

}
