package com.ycl.blog.designmode.factory.factorymethod;


/**
 * User: 杨成龙
 * Date: 2020/3/24
 * Time: 11:02 下午
 * Desc: 平安保险公司
 */
public class PaOrder implements IOrder {
    @Override
    public void createOrder() {
        System.out.println("平安保险下单");
    }
}
