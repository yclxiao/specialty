package com.ycl.blog.designmode.factory.factorymethod;


/**
 * User: 杨成龙
 * Date: 2020/3/24
 * Time: 11:02 下午
 * Desc: 人保保险公司
 */
public class RbOrder implements IOrder {
    @Override
    public void createOrder() {
        System.out.println("人保保险下单");
    }
}
