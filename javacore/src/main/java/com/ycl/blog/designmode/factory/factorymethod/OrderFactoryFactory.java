package com.ycl.blog.designmode.factory.factorymethod;

import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * User: 杨成龙
 * Date: 2020/3/24
 * Time: 11:03 下午
 * Desc: 用来创建工厂对象的  工厂，适合创建负责对象
 */
public class OrderFactoryFactory {
    private static Map<String, IOrderFactory> cachedCompanyFactory = new HashMap<>();

    static {
        cachedCompanyFactory.put("pa", new PaOrderFactory());
        cachedCompanyFactory.put("rb", new RbOrderFactory());
        cachedCompanyFactory.put("tpy", new TpyOrderFactory());
    }

    public static IOrderFactory getInsuranceOrder(String company) {
        if (StringUtils.isEmpty(company)) {
            throw new ArithmeticException("保险公司名称不能为空");
        }
        return cachedCompanyFactory.get(company);
    }
}
