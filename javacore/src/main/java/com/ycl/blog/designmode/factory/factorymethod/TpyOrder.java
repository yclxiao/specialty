package com.ycl.blog.designmode.factory.factorymethod;

/**
 * User: 杨成龙
 * Date: 2020/3/24
 * Time: 11:02 下午
 * Desc: 太平洋保险公司
 */
public class TpyOrder implements IOrder {
    @Override
    public void createOrder() {
        System.out.println("太平洋保险下单");
    }
}
