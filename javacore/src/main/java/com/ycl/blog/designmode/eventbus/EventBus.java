package com.ycl.blog.designmode.eventbus;

import java.util.List;
import java.util.concurrent.Executor;

/**
 * User: 杨成龙
 * Date: 2020/4/13
 * Time: 9:16 上午
 * Desc: 消息总线入口方法
 */
public class EventBus {

    private ObserverRegistry registry = new ObserverRegistry();

    private Executor executor;

    public EventBus() {

    }

    public EventBus(Executor executor) {
        this.executor = executor;
    }

    /**
     * 注册观察者
     */
    public void register(Object observer) {
        registry.register(observer);
    }

    /**
     * 发布者-发送消息
     */
    public void post(Object event) {
        List<ObserverAction> observerActions = registry.getMatchedObserverActions(event);
        for (ObserverAction observerAction : observerActions) {
            if (executor == null) {
                observerAction.execute(event);
            } else {
                executor.execute(() -> {
                    observerAction.execute(event);
                });
            }
        }
    }
}
