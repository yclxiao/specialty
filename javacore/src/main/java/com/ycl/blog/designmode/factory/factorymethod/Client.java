package com.ycl.blog.designmode.factory.factorymethod;


/**
 * User: 杨成龙
 * Date: 2020/3/24
 * Time: 11:07 下午
 * Desc: 简单工厂调用入口类
 */
public class Client {
    public static void main(String[] args) {
        IOrderFactory iOrderFactory = OrderFactoryFactory.getInsuranceOrder("pa");
        IOrder iOrder = iOrderFactory.createInsuranceOrder();
        iOrder.createOrder();
    }
}
