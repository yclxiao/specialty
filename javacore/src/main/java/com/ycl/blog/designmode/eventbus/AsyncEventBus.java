package com.ycl.blog.designmode.eventbus;

import java.util.List;
import java.util.concurrent.Executor;

/**
 * User: 杨成龙
 * Date: 2020/4/13
 * Time: 9:16 上午
 * Desc: 异步消息总线入口方法
 */
public class AsyncEventBus extends EventBus {
    public AsyncEventBus(Executor executor) {
        super(executor);
    }
}
