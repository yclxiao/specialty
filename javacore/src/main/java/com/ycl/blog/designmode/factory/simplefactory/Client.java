package com.ycl.blog.designmode.factory.simplefactory;

/**
 * User: 杨成龙
 * Date: 2020/3/24
 * Time: 11:07 下午
 * Desc: 简单工厂调用入口类
 */
public class Client {
    public static void main(String[] args) {
        IOrder iOrder = OrderFactory.getInsuranceOrder("pa");
        iOrder.createOrder();
    }
}
