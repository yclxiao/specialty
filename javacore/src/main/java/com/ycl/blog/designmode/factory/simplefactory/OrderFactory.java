package com.ycl.blog.designmode.factory.simplefactory;

import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * User: 杨成龙
 * Date: 2020/3/24
 * Time: 11:03 下午
 * Desc: 类描述
 */
public class OrderFactory {
    private static Map<String, IOrder> cachedCompany = new HashMap<>();

    static {
        cachedCompany.put("pa", new PaOrder());
        cachedCompany.put("rb", new RbOrder());
        cachedCompany.put("tpy", new TpyOrder());
    }

    public static IOrder getInsuranceOrder(String company) {
        if (StringUtils.isEmpty(company)) {
            throw new ArithmeticException("保险公司名称不能为空");
        }
        return cachedCompany.get(company);
    }
}
