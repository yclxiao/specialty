package com.ycl.blog.designmode.eventbus;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * User: 杨成龙
 * Date: 2020/4/13
 * Time: 9:13 上午
 * Desc: 注解，用来标识哪些观察者的方法可以执行
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Subscribe {
}
