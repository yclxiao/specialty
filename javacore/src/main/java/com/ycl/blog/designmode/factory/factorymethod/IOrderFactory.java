package com.ycl.blog.designmode.factory.factorymethod;

/**
 * User: 杨成龙
 * Date: 2020/3/24
 * Time: 11:01 下午
 * Desc: 保险公司下单接口
 */
public interface IOrderFactory {
    IOrder createInsuranceOrder();
}
