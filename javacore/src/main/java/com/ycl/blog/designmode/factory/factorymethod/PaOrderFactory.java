package com.ycl.blog.designmode.factory.factorymethod;

/**
 * User: 杨成龙
 * Date: 2020/3/24
 * Time: 11:02 下午
 * Desc: 平安保险公司
 */
public class PaOrderFactory implements IOrderFactory {

    @Override
    public IOrder createInsuranceOrder() {
        System.out.println("工厂方法模式比较适合，创建复杂对象的场景使用");
        System.out.println("比如需要设置很多参数，还需要记录日志等一系列动作");
        return new PaOrder();
    }
}
