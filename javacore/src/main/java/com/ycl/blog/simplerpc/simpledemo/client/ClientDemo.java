package com.ycl.blog.simplerpc.simpledemo.client;

import com.ycl.blog.simplerpc.RpcClient;

/**
 * User: 杨成龙
 * Date: 2020/4/13
 * Time: 8:13 下午
 * Desc: 类描述
 */
public class ClientDemo {
    public static void main(String[] args) throws Exception {
        RpcClient client = new RpcClient();
        CalculatorService service = client.refer(CalculatorService.class, "127.0.0.1", 1234);
        int result = service.add(2, 4);
        System.out.println("result:" + result);
    }
}
