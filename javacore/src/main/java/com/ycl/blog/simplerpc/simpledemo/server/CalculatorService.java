package com.ycl.blog.simplerpc.simpledemo.server;

/**
 * User: 杨成龙
 * Date: 2020/4/13
 * Time: 8:12 下午
 * Desc: 类描述
 */
public interface CalculatorService {
    int add(int a, int b);
}
