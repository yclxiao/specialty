package com.ycl.blog.simplerpc.simpledemo.server;

import com.ycl.blog.simplerpc.RpcServer;

/**
 * User: 杨成龙
 * Date: 2020/4/13
 * Time: 8:16 下午
 * Desc: 类描述
 */
public class RpcServerApp {
    public static void main(String[] args) throws Exception {
        CalculatorService service = new CalculatorServiceImpl();
        RpcServer server = new RpcServer();
        server.export(service, 1234);
    }
}
