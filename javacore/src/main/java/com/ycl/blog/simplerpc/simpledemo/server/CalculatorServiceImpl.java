package com.ycl.blog.simplerpc.simpledemo.server;

/**
 * User: 杨成龙
 * Date: 2020/4/13
 * Time: 8:15 下午
 * Desc: 类描述
 */
public class CalculatorServiceImpl implements CalculatorService {
    @Override
    public int add(int a, int b) {
        return a + b;
    }
}
