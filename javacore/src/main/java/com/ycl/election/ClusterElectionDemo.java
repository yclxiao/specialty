package com.ycl.election;

public class ClusterElectionDemo {
    public static void main(String[] args) throws Exception {
        // 启动多个节点，模拟集群
        int totalNodes = 3; // 集群节点总数

        ElectionNode node1 = new ElectionNode(1, 101, "localhost", 9001, totalNodes);
        Constant.nodeMap.put(1, node1);
        node1.start();
    }
}
