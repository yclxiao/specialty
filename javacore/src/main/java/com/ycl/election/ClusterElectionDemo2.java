package com.ycl.election;

public class ClusterElectionDemo2 {
    public static void main(String[] args) throws Exception {
        // 启动多个节点，模拟集群
        int totalNodes = 3; // 集群节点总数

        ElectionNode node2 = new ElectionNode(2, 102, "localhost", 9002, totalNodes);
        Constant.nodeMap.put(2, node2);
        node2.start();
    }
}
