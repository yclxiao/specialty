package com.ycl.election;

import java.io.Serializable;

public class ElectionMessage implements Serializable {
    public enum MessageType {
        VOTE_REQUEST, // 投票请求
        VOTE,         // 投票
        ELECTED       // 选举完成后的胜出消息
    }

    private MessageType type;
    private int nodeId;   // 节点ID
    private long zxId;    // ZXID：类似于ZooKeeper中的逻辑时钟，用于比较
    private int voteFor;  // 投票给的节点ID

    public ElectionMessage(MessageType type, int nodeId, long zxId, int voteFor) {
        this.type = type;
        this.nodeId = nodeId;
        this.zxId = zxId;
        this.voteFor = voteFor;
    }

    // Getters and setters
    public MessageType getType() {
        return type;
    }

    public int getNodeId() {
        return nodeId;
    }

    public long getZxId() {
        return zxId;
    }

    public int getVoteFor() {
        return voteFor;
    }

    @Override
    public String toString() {
        return "ElectionMessage{" +
                "type=" + type +
                ", nodeId=" + nodeId +
                ", zxId=" + zxId +
                ", voteFor=" + voteFor +
                '}';
    }
}
