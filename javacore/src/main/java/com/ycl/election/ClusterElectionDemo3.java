package com.ycl.election;

public class ClusterElectionDemo3 {
    public static void main(String[] args) throws Exception {
        // 启动多个节点，模拟集群
        int totalNodes = 3; // 集群节点总数

        ElectionNode node3 = new ElectionNode(3, 103, "localhost", 9003, totalNodes);
        Constant.nodeMap.put(3, node3);
        node3.start();
    }
}
