package com.ycl.javacore.byteopt;

/**
 * User: OF1089 杨成龙
 * Date: 2020/2/19
 * Time: 9:43 上午
 * Desc: 类描述
 */
public class ByteTest {
    public static int getHeight4(byte data) {
        int height;
        height = ((data & 0xf0) >> 4);
        return height;
    }

    public static int getLow4(byte data) {
        int low;
        low = (data & 0x0f);
        return low;
    }

    public static void main(String[] args) {
        byte a = 127;
        int height = getHeight4(a);
        int low = getLow4(a);
        System.out.println(height);
        System.out.println(low);
    }
}
