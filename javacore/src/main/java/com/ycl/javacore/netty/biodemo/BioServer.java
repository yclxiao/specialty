package com.ycl.javacore.netty.biodemo;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * User: OF1089 杨成龙
 * Date: 2019/7/11
 * Time: 11:10 AM
 * Desc: 类描述
 */
public class BioServer {
    public static void main(String[] args) {
        int port = 8080;
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(port);
            Socket socket;
            while (true) {
                socket = serverSocket.accept();
                new Thread(new TimeServerHandler(socket)).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (serverSocket != null) {
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
