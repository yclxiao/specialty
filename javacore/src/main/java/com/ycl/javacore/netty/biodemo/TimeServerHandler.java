package com.ycl.javacore.netty.biodemo;

import java.io.*;
import java.net.Socket;
import java.util.Date;

/**
 * User: OF1089 杨成龙
 * Date: 2019/7/11
 * Time: 11:14 AM
 * Desc: 类描述
 */
public class TimeServerHandler implements Runnable {
    private Socket socket;

    public TimeServerHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        BufferedReader in = null;
        PrintStream out = null;

        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintStream(socket.getOutputStream(),true);

            String body = null;
            while ((body = in.readLine()) != null) {
                System.out.println(body);
                out.println(new Date().toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (out != null) {
                out.close();
            }
            if (this.socket != null) {
                try {
                    this.socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
