package com.ycl.javacore.array;

import lombok.Data;

/**
 * User: OF1089 杨成龙
 * Date: 2019/11/2
 * Time: 2:41 PM
 * Desc: 类描述
 */
@Data
public class Student {
    private String name;
    private int score;
}
