package com.ycl.javacore.array;

import org.junit.Test;

/**
 * User: OF1089 杨成龙
 * Date: 2019/8/8
 * Time: 1:50 PM
 * Desc: 二维数组
 */
public class TwoDimension {
    /**
     * 遍历二维数组
     */
    @Test
    public void iteratorTwoDimension() {
        int[][] key = {{1, 2}, {4, 2, 3}, {59, 90}};
        for (int i = 0, length = key.length; i < length; i++) {
            int[] keyArray = key[i];
            for (int j = 0, arrayLength = keyArray.length; j < arrayLength; j++) {
                System.out.print(keyArray[j] + " ");
            }
            System.out.println("");
        }

        int[][] twoDi = new int[2][3];

    }
}
