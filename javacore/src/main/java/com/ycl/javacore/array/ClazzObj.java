package com.ycl.javacore.array;

import lombok.Data;

import java.util.List;

/**
 * User: OF1089 杨成龙
 * Date: 2019/11/2
 * Time: 2:40 PM
 * Desc: 班级对象
 */
@Data
public class ClazzObj {
    private String name;
    private List<Student> studentList;
}
