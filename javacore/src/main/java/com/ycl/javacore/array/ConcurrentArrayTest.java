package com.ycl.javacore.array;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * User: OF1089 杨成龙
 * Date: 2019/11/2
 * Time: 2:39 PM
 * Desc: 类描述
 */
public class ConcurrentArrayTest {
    public static void main(String[] args) {


        ConcurrentArrayTest test = new ConcurrentArrayTest();

        Random random = new Random();
        ClazzObj clazzObj = new ClazzObj();
        clazzObj.setName("一年级");
        List<Student> studentList = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            Student temp = new Student();
            temp.setName("name" + i);
            temp.setScore(random.nextInt(100));
            studentList.add(temp);
        }
        clazzObj.setStudentList(studentList);


        for (int i = 0; i < 1000; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    test.sortAndGet(clazzObj);
                }
            }).start();
        }

    }

    private void sortAndGet(ClazzObj clazzObj) {
        List<Student> studentListAlone = new ArrayList<>();
        studentListAlone.addAll(clazzObj.getStudentList());
        studentListAlone.sort((a,b) -> {
            if (a.getScore() > b.getScore()) {
                return 1;
            } else if (a.getScore() < b.getScore()) {
                return -1;
            }
            return 0;
        });
        clazzObj.setStudentList(studentListAlone);

        for (Student student : clazzObj.getStudentList()) {
            System.out.println(Thread.currentThread().getName() + " -------- " +student.getScore());
        }

        /*clazzObj.getStudentList().sort((a,b) -> {
            if (a.getScore() > b.getScore()) {
                return 1;
            } else if (a.getScore() < b.getScore()) {
                return -1;
            }
            return 0;
        });

        *//*for (Student student : clazzObj.getStudentList()) {
            System.out.println(student.getName());
        }*//*

        Iterator<Student> iterator = clazzObj.getStudentList().iterator();
        while (iterator.hasNext()) {
            Student temp = iterator.next();
            System.out.println(temp.getName());
        }*/
    }
}
