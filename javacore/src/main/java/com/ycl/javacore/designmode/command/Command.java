package com.ycl.javacore.designmode.command;

/**
 * User: OF1089 杨成龙
 * Date: 2020/3/15
 * Time: 9:08 下午
 * Desc: 命令接口
 */
public interface Command {
    void exe();
}
