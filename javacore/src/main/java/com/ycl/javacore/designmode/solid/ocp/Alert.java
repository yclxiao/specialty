package com.ycl.javacore.designmode.solid.ocp;

import java.util.ArrayList;
import java.util.List;

/**
 * User: OF1089 杨成龙
 * Date: 2020/1/28
 * Time: 5:31 下午
 * Desc: 报警处理类
 */
public class Alert {

    private List<AlertHandler> alertHandlerList = new ArrayList<>();

    /**
     * 加入所有报警处理方式
     *
     * @param alertHandler
     */
    public void addAlertHandler(AlertHandler alertHandler) {
        this.alertHandlerList.add(alertHandler);
    }

    /**
     * 对某个api，每个报警方式处理一遍
     *
     * @param apiStatInfo
     */
    public void check(ApiStatInfo apiStatInfo) {
        for (AlertHandler alertHandler : alertHandlerList) {
            alertHandler.check(apiStatInfo);
        }
    }
}
