package com.ycl.javacore.designmode.command;

/**
 * User: OF1089 杨成龙
 * Date: 2020/3/15
 * Time: 9:09 下午
 * Desc: 类描述
 */
public class Receiver {
    public void action() {
        System.out.println("command received!");
    }
}
