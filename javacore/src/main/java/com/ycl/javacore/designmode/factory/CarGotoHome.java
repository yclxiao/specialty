package com.ycl.javacore.designmode.factory;

/**
 * User: OF1089 杨成龙
 * Date: 2020/2/17
 * Time: 10:37 上午
 * Desc: 类描述
 */
public class CarGotoHome extends AbstractGotoHome {

    @Override
    public void goHome() {
        System.out.println("go home by car");
    }

    @Override
    String getTool() {
        return "car";
    }
}
