package com.ycl.javacore.designmode.adapter;

/**
 * User: OF1089 杨成龙
 * Date: 2019/7/10
 * Time: 5:05 PM
 * Desc: 类描述
 */
public class ChargeAdapter implements LightningInterface {
    TypeCInterface typeCInterface;

    public ChargeAdapter(TypeCInterface typeCInterface) {
        this.typeCInterface = typeCInterface;
    }

    @Override
    public void chargeWithLightning() {
        typeCInterface.chargeWithTypeC();
    }
}
