package com.ycl.javacore.designmode.solid.ocp;

/**
 * User: OF1089 杨成龙
 * Date: 2020/1/28
 * Time: 5:35 下午
 * Desc: TPS报警处理
 */
public class TpsAlertHandler extends AlertHandler {
    public TpsAlertHandler(AlertRule alertRule, Notification notification) {
        super(alertRule, notification);
    }

    @Override
    public void check(ApiStatInfo apiStatInfo) {
        long tps = apiStatInfo.getRequestCount() / apiStatInfo.getDurationOfSeconds();
        if (tps > alertRule.getMatchRule(apiStatInfo.getApi()).getMaxTps()) {
            notification.notifyChannel();
        }
    }
}
