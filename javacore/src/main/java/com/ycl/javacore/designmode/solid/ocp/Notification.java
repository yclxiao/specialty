package com.ycl.javacore.designmode.solid.ocp;

/**
 * User: OF1089 杨成龙
 * Date: 2020/1/28
 * Time: 5:32 下午
 * Desc: 报警之后的通知
 */
public class Notification {
    public void notifyChannel() {
        System.out.println("notify");
    }
}
