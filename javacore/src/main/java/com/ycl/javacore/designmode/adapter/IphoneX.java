package com.ycl.javacore.designmode.adapter;

/**
 * User: OF1089 杨成龙
 * Date: 2019/7/10
 * Time: 5:01 PM
 * Desc: 类描述
 */
public class IphoneX {
    LightningInterface lightningInterface;

    public IphoneX(LightningInterface lightningInterface) {
        this.lightningInterface = lightningInterface;
    }

    public void charge() {
        System.out.println("开始给我的iphone充电");
        lightningInterface.chargeWithLightning();
        System.out.println("结束我的iphone充电");
    }
}
