package com.ycl.javacore.designmode.adapter;

/**
 * User: OF1089 杨成龙
 * Date: 2019/7/10
 * Time: 5:04 PM
 * Desc: 类描述
 */
public class Test {
    public static void main(String[] args) {
        ChargeAdapter chargeAdapter = new ChargeAdapter(new AndroidCharger());
        IphoneX iphoneX = new IphoneX(chargeAdapter);
        iphoneX.charge();
    }
}
