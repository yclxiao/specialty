package com.ycl.javacore.designmode.adapter;

/**
 * User: OF1089 杨成龙
 * Date: 2019/7/10
 * Time: 5:00 PM
 * Desc: 类描述
 */
public interface LightningInterface {
    void chargeWithLightning();
}
