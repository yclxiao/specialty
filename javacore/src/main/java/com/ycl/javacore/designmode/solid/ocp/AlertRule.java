package com.ycl.javacore.designmode.solid.ocp;

import lombok.Data;

/**
 * User: OF1089 杨成龙
 * Date: 2020/1/28
 * Time: 5:33 下午
 * Desc: 报警规则
 */
@Data
public class AlertRule {
    /**
     * 最大错误请求数字
     */
    private long maxErrorCount;
    /**
     * 最大TPS
     */
    private long maxTps;
    /**
     * 最大超时时间-毫秒
     */
    private long maxTimeout;

    public AlertRule getMatchRule(String api) {
        AlertRule alertRule = new AlertRule();
        //根据api查询他对应的，报警规则，报警规则可以放在一张表里
        return alertRule;
    }

}
