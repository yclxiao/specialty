package com.ycl.javacore.designmode.solid.ocp;

/**
 * User: OF1089 杨成龙
 * Date: 2020/1/28
 * Time: 5:56 下午
 * Desc: 入口类
 */
public class ApplicationContext {

    private Alert alert;
    private AlertRule alertRule;
    private Notification notification;

    public void initializeBeans() {
        alert = new Alert();
        alertRule = new AlertRule();
        notification = new Notification();
        alert.addAlertHandler(new TpsAlertHandler(alertRule, notification));
    }

    public Alert getAlert() {
        return alert;
    }

    private static final ApplicationContext instance = new ApplicationContext();

    private ApplicationContext() {
        instance.initializeBeans();
    }

    public static ApplicationContext getInstance() {
        return instance;
    }

    public static void main(String[] args) {
        //比如有一处存了api的调用情况，然后取出来，然后校验他是否需要报警
        ApiStatInfo apiStatInfo = new ApiStatInfo();
        ApplicationContext.getInstance().getAlert().check(apiStatInfo);
    }
}
