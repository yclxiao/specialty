package com.ycl.javacore.designmode.factory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * User: OF1089 杨成龙
 * Date: 2020/2/17
 * Time: 10:34 上午
 * Desc: 类描述
 */
public class Test {
    Map<String, GotoHome> toolMap = new ConcurrentHashMap<>();

    public void putTool(String tool, GotoHome gotoHome) {
        toolMap.put(tool, gotoHome);
    }

    private Test() {
    }

    private static Test instance = new Test();

    public static Test getInstance() {
        return instance;
    }

    public static void main(String[] args) {
        Test test = Test.getInstance();
        new CarGotoHome();
        new TrainGotoHome();

        GotoHome gotoHome = test.toolMap.get("train");
        gotoHome.goHome();
    }
}
