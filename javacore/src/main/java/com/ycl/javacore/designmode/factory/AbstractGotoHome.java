package com.ycl.javacore.designmode.factory;

import javax.annotation.PostConstruct;

/**
 * User: OF1089 杨成龙
 * Date: 2020/2/17
 * Time: 10:32 上午
 * Desc: 类描述
 */
public abstract class AbstractGotoHome implements GotoHome {

    Test test = Test.getInstance();

    public AbstractGotoHome() {
        test.putTool(getTool(), this);
    }

    abstract String getTool();

}
