package com.ycl.javacore.designmode.solid.ocp;

import lombok.Data;

/**
 * User: OF1089 杨成龙
 * Date: 2020/1/28
 * Time: 5:31 下午
 * Desc: 调用的api的状态信息
 */
@Data
public class ApiStatInfo {

    private String api;

    private long requestCount;

    private long errorCount;

    private long durationOfSeconds;
}
