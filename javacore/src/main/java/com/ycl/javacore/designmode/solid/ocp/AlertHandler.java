package com.ycl.javacore.designmode.solid.ocp;

/**
 * User: OF1089 杨成龙
 * Date: 2020/1/28
 * Time: 5:34 下午
 * Desc: 报警处理方法的抽象类
 */
public abstract class AlertHandler {
    protected AlertRule alertRule;

    protected Notification notification;

    public AlertHandler(AlertRule alertRule, Notification notification) {
        this.alertRule = alertRule;
        this.notification = notification;
    }

    /**
     * 抽象出检查的方法，让不同子类实现
     *
     * @param apiStatInfo
     */
    public abstract void check(ApiStatInfo apiStatInfo);
}
