package com.ycl.javacore.designmode.command;

/**
 * User: OF1089 杨成龙
 * Date: 2020/3/15
 * Time: 9:10 下午
 * Desc: 类描述
 */
public class Invoker {
    private Command command;

    public Invoker(Command command) {
        this.command = command;
    }

    public void action() {
        command.exe();
    }
}
