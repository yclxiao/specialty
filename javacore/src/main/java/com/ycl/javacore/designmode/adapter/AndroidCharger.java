package com.ycl.javacore.designmode.adapter;

/**
 * User: OF1089 杨成龙
 * Date: 2019/7/10
 * Time: 5:03 PM
 * Desc: 类描述
 */
public class AndroidCharger implements TypeCInterface {
    @Override
    public void chargeWithTypeC() {
        System.out.println("使用Type-C类型的充电器充电");
    }
}
