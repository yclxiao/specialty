package com.ycl.javacore.designmode.factory;

/**
 * User: OF1089 杨成龙
 * Date: 2020/2/17
 * Time: 10:47 上午
 * Desc: 类描述
 */
public class TrainGotoHome extends AbstractGotoHome {
    @Override
    String getTool() {
        return "train";
    }

    @Override
    public void goHome() {
        System.out.println("train go to home");
    }
}
