package com.ycl.javacore.designmode.testdata;

import com.google.common.annotations.VisibleForTesting;

/**
 * User: OF1089 杨成龙
 * Date: 2020/2/2
 * Time: 5:39 下午
 * Desc: 类描述
 */
public class PrivateData {

    @VisibleForTesting
    protected void testPrivate(String in) {
        System.out.println(in);
    }
}
