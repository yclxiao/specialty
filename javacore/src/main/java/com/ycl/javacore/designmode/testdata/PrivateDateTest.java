package com.ycl.javacore.designmode.testdata;

import org.junit.Test;

/**
 * User: OF1089 杨成龙
 * Date: 2020/2/2
 * Time: 5:43 下午
 * Desc: 类描述
 */
public class PrivateDateTest {

    @Test
    public void testPrivate() throws Exception {
        PrivateData privateData = new PrivateData();
        privateData.testPrivate("111");
    }
}
