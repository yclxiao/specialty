package com.ycl.javacore.designmode.command;

/**
 * User: OF1089 杨成龙
 * Date: 2020/3/15
 * Time: 9:08 下午
 * Desc: 类描述
 */
public class MyCommand implements Command {

    private Receiver receiver;

    public MyCommand(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void exe() {
        receiver.action();
    }
}
