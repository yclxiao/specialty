package com.ycl.javacore.designmode.command;

/**
 * User: OF1089 杨成龙
 * Date: 2020/3/15
 * Time: 9:11 下午
 * Desc: 命令模式的目的就是达到命令的发出者和执行者之间解耦，实现请求和执行分开
 */
public class Test {
    public static void main(String[] args) {
        Receiver receiver = new Receiver();//执行者
        Command command = new MyCommand(receiver);//命令
        Invoker invoker = new Invoker(command);//命令的发出者
        invoker.action();
    }
}
