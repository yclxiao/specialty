package com.ycl.javacore.thread;

import java.util.concurrent.CountDownLatch;

/**
 * User: OF1089 杨成龙
 * Date: 2019/3/23
 * Time: 8:15 PM
 * Desc: 类描述
 */
public class CountDownLatchTest {
    private static CountDownLatch countDownLatch = new CountDownLatch(7);

    public static void main(String[] args) {
        for (int i = 0; i < 7; i++) {
            int finalI = i;
            new Thread(() -> {
                System.out.println("第" + finalI + "龙珠已经收集");
                countDownLatch.countDown();
            }).start();
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("龙珠已经集齐");
    }
}
