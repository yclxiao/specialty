package com.ycl.javacore.thread;

import java.util.concurrent.*;

/**
 * User: OF1089 杨成龙
 * Date: 2019/3/25
 * Time: 10:38 AM
 * Desc: 类描述
 */
public class ThreadPoolTest implements Runnable {
    @Override
    public void run() {
        synchronized (this) {
            try {
                System.out.println(Thread.currentThread().getName());
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        //尽量不要使用Executors去创建线程池
        BlockingDeque<Runnable> blockingDeque = new LinkedBlockingDeque<>(4);
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(2,6,1, TimeUnit.DAYS,blockingDeque);


        for (int i = 0; i < 10; i++) {
            threadPoolExecutor.execute(new Thread(new ThreadPoolTest(),"TestThread".concat("" + i)));
            int queueSize = blockingDeque.size();
            System.out.println("队列大小为：" + queueSize);
        }

        threadPoolExecutor.shutdown(); //拒绝接受新的任务
        threadPoolExecutor.awaitTermination(30,TimeUnit.SECONDS);//等待30S停止线程池

//        ExecutorService executorService = Executors.newCachedThreadPool();
//        executorService.submit()

    }

}
