package com.ycl.javacore.thread.threadlocal;

import java.util.concurrent.CountDownLatch;

/**
 * User: OF1089 杨成龙
 * Date: 2020/2/3
 * Time: 9:17 上午
 * Desc: 类描述
 */
public class MyStringDemoThreadLocal {
    private static ThreadLocal<String> threadLocal = new ThreadLocal<>();

    private void setString(String str) {
        threadLocal.set(str);
    }

    private String getString() {
        return threadLocal.get();
    }

    public static void main(String[] args) {
        MyStringDemoThreadLocal demo = new MyStringDemoThreadLocal();
        int threads = 9;
        CountDownLatch latch = new CountDownLatch(threads);
        for (int i = 0; i < 9; i++) {
            Thread thread = new Thread(() -> {
                demo.setString(Thread.currentThread().getName());
                System.out.println(demo.getString());
                latch.countDown();
            }, "thread - " + i);
            thread.start();
        }
    }
}
