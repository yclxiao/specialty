package com.ycl.javacore.thread;

import java.util.concurrent.atomic.AtomicLong;

/**
 * User: OF1089 杨成龙
 * Date: 2019/3/23
 * Time: 8:26 PM
 * Desc: 类描述
 */
public class JoinTest {
    public static void main(String[] args) throws InterruptedException {
        /*Thread t = new Thread(() -> {

            System.out.println("A begin");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("A end");
        });

        t.start();

        t.join();

        System.out.println("main ready end");
        Thread.sleep(2000);
        System.out.println("main end");*/

        /*List<String> list = new ArrayList<>();
        for (int i = 0; i < 10000000; i++) {
            String temp = i + "";
            list.add(temp);
        }*/


        /*ExecutorService executorService = Executors.newFixedThreadPool(6);
        executorService.submit(() -> {

        });*/


        AtomicLong atomicLong = new AtomicLong(0);

        for (int i = 0; i < 100; i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    atomicLong.incrementAndGet();
                }
            });
            thread.start();

            thread.join();
            System.out.println("创建线程：" + i);
        }

        System.out.println("end");

        System.out.println(atomicLong.get());


    }
}
