package com.ycl.javacore.thread;

import java.util.concurrent.*;

/**
 * User: OF1089 杨成龙
 * Date: 2019/3/23
 * Time: 7:54 PM
 * Desc: 类描述
 */
public class SemaphoreTest {

    private static Semaphore s = new Semaphore(10);
    private static ExecutorService threadPool = Executors.newFixedThreadPool(30);

    public static void main(String[] args) {
        for (int i = 0; i<30; i++) {
            /*threadPool.submit(() -> {
                try {
                    s.acquire();
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("save data");
                s.release();
            });*/

            Future<Integer> future = threadPool.submit(new Callable<Integer>() {
                @Override
                public Integer call() throws Exception {
                    System.out.println("=====");
                    return 1;
                }
            });

            Integer result = null;
            try {
                result = future.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            System.out.println(result);
        }

        threadPool.shutdown();


    }
}
