package com.ycl.javacore.thread.interrupt;

/**
 * User: OF1089 杨成龙
 * Date: 2020/3/15
 * Time: 7:45 下午
 * Desc: 中断线程
 */
public class InterruptThread extends Thread {
    @Override
    public void run() {
        super.run();
        try {
            for (int i = 0; i < 500000; i++) {
                if (this.interrupted()) {
                    System.out.println("线程已经终止， for循环不再执行");
                    throw new InterruptedException();
                }

                System.out.println("i=" + (i + 1));
            }

            System.out.println("这是for循环外面的语句，也会被执行");
        } catch (InterruptedException e) {
            System.out.println("进入MyThread.java类中的catch了。。。");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        InterruptThread thread = new InterruptThread();
        thread.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(thread.getName());
        System.out.println(Thread.currentThread().getName());

        thread.interrupt();
//        Thread.currentThread().interrupt(); //没用
    }
}
