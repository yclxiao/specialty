package com.ycl.javacore.thread.atomic;

import java.util.concurrent.atomic.AtomicReference;

/**
 * User: OF1089 杨成龙
 * Date: 2020/3/12
 * Time: 2:19 下午
 * Desc: 类描述
 */
public class AtomicReferenceTest {
    public static void main(String[] args) {
        String initialReference = "initial value referenced";

        AtomicReference<String> atomicStringReference = new AtomicReference<String>(initialReference);

        String newReference = "new value referenced";
        boolean exchanged = atomicStringReference.compareAndSet(initialReference, newReference);
        System.out.println("exchanged: " + exchanged);

        exchanged = atomicStringReference.compareAndSet(initialReference, newReference);
        System.out.println("exchanged: " + exchanged);
    }
}
