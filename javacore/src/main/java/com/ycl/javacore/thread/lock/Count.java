package com.ycl.javacore.thread.lock;

/**
 * User: OF1089 杨成龙
 * Date: 2019/4/5
 * Time: 4:31 PM
 * Desc: 类描述
 */
public class Count {
//    SelfLock selfLock = new SelfLock();
    SelfReentrantLock selfLock = new SelfReentrantLock();

    public void print() throws InterruptedException {
        selfLock.lock();
        doAdd();
        selfLock.unlock();
    }

    public void doAdd() throws InterruptedException {
        selfLock.lock();
        System.out.println("...doAdd...");
        selfLock.lock();
    }

    public static void main(String[] args) throws InterruptedException {
        Count count = new Count();
        count.print();
    }
}
