package com.ycl.javacore.thread.lock;

/**
 * User: OF1089 杨成龙
 * Date: 2019/4/5
 * Time: 4:23 PM
 * Desc: 类描述
 */
public class SelfLock {

    private boolean isLock;

    public synchronized void lock() throws InterruptedException {
        //一定要用while，不能用if。。。因为一旦线程被notify唤醒，可能有多个线程再争抢资源，资源不一定被某个线程抢到
        //所以线程被唤醒的时候一定要再用while再看下，资源有没有被抢走，如果while确定没有被抢走，则可以被他抢过来
        //如果线程被唤醒，发现资源被别的线程抢走了，那他继续等待
        while (isLock) {
            wait();
        }
        isLock = true;
    }

    public synchronized void unlock() {
        isLock = false;
        notify();
    }
}
