package com.ycl.javacore.thread.threadlocal;

import java.util.concurrent.CountDownLatch;

/**
 * User: OF1089 杨成龙
 * Date: 2020/2/3
 * Time: 8:44 上午
 * Desc: 类描述
 */
public class MyStringDemo {
    private String str;

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public static void main(String[] args) {
        MyStringDemo demo = new MyStringDemo();
        int threads = 9;
        CountDownLatch latch = new CountDownLatch(threads);
        for (int i = 0; i < threads; i++) {
            Thread thread = new Thread(() -> {
                demo.setStr(Thread.currentThread().getName());
                System.out.println(demo.getStr());
                latch.countDown();
            }, "thread - " + i);
            thread.start();
        }
    }


}
