package com.ycl.javacore.thread.atomic;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * User: OF1089 杨成龙
 * Date: 2020/2/18
 * Time: 9:15 上午
 * Desc: 类描述
 */
public class AtomicTest {
    AtomicInteger count = new AtomicInteger(0);

    int put() {
        return count.incrementAndGet();
    }
    int get() {
        return count.get();
    }

    public static void main(String[] args) {
        AtomicTest atomicTest = new AtomicTest();
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                for (int j = 0; j < 1000; j++) {
                    atomicTest.put();
                }
            }).start();
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(atomicTest.get());
    }
}
