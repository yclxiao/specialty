package com.ycl.javacore.thread;

import java.util.concurrent.*;

/**
 * User: OF1089 杨成龙
 * Date: 2020/2/6
 * Time: 4:46 下午
 * Desc: 类描述
 */
public class ThreadPoolTest2 {
    BlockingQueue blockingQueue = new ArrayBlockingQueue(10);
    ExecutorService executorService = new ThreadPoolExecutor(5, 10, 5,
            TimeUnit.MINUTES, blockingQueue, new ThreadPoolExecutor.AbortPolicy());

    public void executeMethod() {
        System.out.println("1111");
    }

    public static void main(String[] args) throws InterruptedException {
        ThreadPoolTest2 threadPoolTest2 = new ThreadPoolTest2();
        int size = 15;
        for (int i = 0; i < size; i++) {
            threadPoolTest2.executorService.submit(() -> {
                threadPoolTest2.executeMethod();
            });
        }
    }

    /*public static void main(String[] args) throws InterruptedException {
        int size = 1000;
        long beign = System.currentTimeMillis();
        for (int i = 0; i < size; i++) {
            String temp = "str" + i;
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(temp);
        }
        System.out.println(System.currentTimeMillis() - beign);
    }*/
}
