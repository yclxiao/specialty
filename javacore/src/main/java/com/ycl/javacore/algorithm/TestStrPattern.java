package com.ycl.javacore.algorithm;

import java.util.HashMap;
import java.util.Map;

/**
 * User: OF1089 杨成龙
 * Date: 2019/8/23
 * Time: 7:25 PM
 * Desc: 字符串匹配
 *
 * 题目：
 * 有一个字符串它的构成是词+空格的组合，如“北京 杭州 杭州 北京”， 要求输入一个匹配模式（简单的以字符来写）， 比如 aabb, 来判断该字符串是否符合该模式， 举个例子：
 *
 * 1. pattern = "abba", str="北京 杭州 杭州 北京" 返回 true
 * 2. pattern = "aabb", str="北京 杭州 杭州 北京" 返回 false
 * 3. pattern = "baab", str="北京 杭州 杭州 北京" 返回 true
 *
 */
public class TestStrPattern {

    /**
     * 匹配字符串
     * @param targetStr 目标字符串
     * @param pattern 规则
     * @return
     * 采用map结构存储  ，  key：规则字符串的字符，value：对应位置的目标字符串，
     * 然后根据key取出字符串进行比对，不相等则直接返回false，如果都相等，则返回true
     */
    public static boolean testPattern(String targetStr, String pattern) {
        Map<Character, String> hashMap = new HashMap<>();
        //目标字符串分割成数组
        String[] strArr = targetStr.split(" ");

        for (int i = 0; i < strArr.length; i++) {
            String temp = strArr[i];
            Character c = pattern.charAt(i);
            //如果没有此key，则存入k - v 到map中
            if (hashMap.containsKey(c)) {
                String mapTemp = hashMap.get(c);
                if (!mapTemp.equals(strArr[i])) {
                    return false;
                }
            }
            hashMap.put(c, temp);
        }
        return true;
    }

    public static void main(String[] args) {
        String targetStr = "北京 杭州 杭州 北京";
//        String pattern = "abba";
//        String pattern = "aabb";
        String pattern = "baab";

        boolean result = TestStrPattern.testPattern(targetStr, pattern);
        if (result) {
            System.out.println("结果匹配");
        } else {
            System.out.println("结果不匹配");
        }
    }
}
