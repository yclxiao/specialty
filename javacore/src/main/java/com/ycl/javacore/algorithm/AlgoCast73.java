package com.ycl.javacore.algorithm;

/**
 * User: OF1089 杨成龙
 * Date: 2019/8/25
 * Time: 9:42 AM
 * Desc: 有序链表的去重
 */
public class AlgoCast73 {
    public class ListNode {
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }

    public ListNode removeDuplicatesInSortedList(ListNode head) {
        if (head == null) {
            return null;
        }
        ListNode p = head;
        ListNode q = head.next;

        while (q != null) {
            if (p.val == q.val) {
                p.next = q.next;
                q = q.next;
            } else {
                p = p.next;
                q = q.next;
            }
        }

        return head;
    }
}
