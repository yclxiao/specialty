package com.ycl.javacore.algorithm;

/**
 * Desc: 计算一个整数，二进制表示的1的个数
 */
public class AlgoCasts48 {

    public static class LinkedNode {
        int value;
        LinkedNode next;

        public LinkedNode(int value) {
            this.value = value;
        }
    }

    public static void main(String[] args) {
        LinkedNode linkedNode1 = new LinkedNode(1);
        LinkedNode linkedNode2 = new LinkedNode(2);
        LinkedNode linkedNode3 = new LinkedNode(4);
        LinkedNode linkedNode4 = new LinkedNode(1);
        LinkedNode linkedNode5 = new LinkedNode(8);
        LinkedNode linkedNode6 = new LinkedNode(1);

        linkedNode1.next = linkedNode2;
        linkedNode2.next = linkedNode3;
        linkedNode3.next = linkedNode4;
        linkedNode4.next = linkedNode5;
        linkedNode5.next = linkedNode6;

        AlgoCasts48 algoCasts48 = new AlgoCasts48();
        LinkedNode linkedNode = algoCasts48.delNumInLinkedNode3(linkedNode1, 1);
        while (linkedNode != null) {
            System.out.println(linkedNode.value);
            linkedNode = linkedNode.next;
        }
    }

    private LinkedNode delNumInLinkedNode3(LinkedNode linkedNode, int num) {
        //定义虚拟节点，指向链表头部
        LinkedNode dummy = new LinkedNode(0);
        dummy.next = linkedNode;
        //定义移动节点p，凡是这种需要逐个比对的，一般都需要定义移动指针
        LinkedNode p = dummy;

        //逐个比对
        while (p.next != null) {
            //如果节点的数字 跟 给定的数字相同，则断开此节点
            if (p.next.value == num) {
                p.next = p.next.next;
            }
            //如果不相同，则继续移动指针
            else {
                p = p.next;
            }
        }

        return dummy.next;
    }

}
