package com.ycl.javacore.algorithm;

/**
 * User: OF1089 杨成龙
 * Date: 2020/3/4
 * Time: 8:11 下午
 * Desc: 反转链表
 */
public class AlgoCast10Link {

    /**
     * 节点
     */
    private static class LinkNode {
        private int val;
        private LinkNode next;

        public LinkNode(int val) {
            this.val = val;
        }
    }

    public void reverseLink(LinkNode head) {
        if (head == null) {
            return;
        }

        LinkNode cur = head.next;
        while (cur != null) {
            LinkNode temp = cur.next;
            cur.next = head;
            head = cur;
            cur = temp;
        }
    }

    public static void main(String[] args) {
        AlgoCast10Link link = new AlgoCast10Link();

        AlgoCast10Link.LinkNode node3 = new AlgoCast10Link.LinkNode(3);
        AlgoCast10Link.LinkNode node7 = new AlgoCast10Link.LinkNode(7);
        AlgoCast10Link.LinkNode node5 = new AlgoCast10Link.LinkNode(5);

        node3.next = node7;
        node7.next = node5;

        System.out.println(node3.val + "-" + node3.next.val + "-" + node3.next.next.val);

        link.reverseLink(node3);

        System.out.println(node5.val + "-" + node5.next.val + "-" + node5.next.next.val);
    }
}
