package com.ycl.javacore.algorithm;

import java.util.*;

/**
 * User: OF1089 杨成龙
 * Date: 2019/8/15
 * Time: 4:45 PM
 * Desc: 给定一个数组，求相加=0的3个数
 */
public class AlgoCast53 {
    public List<List<Integer>> threeNumbSum2Zero(int[] nums) {
        int length = nums.length;
        List<List<Integer>> result = new ArrayList<>();
        Set<List<Integer>> tempSet = new HashSet<>();
        for (int i = 0; i < length; i++) {
            for (int j = i + 1; j < length; j++) {
                for (int k = j + 1; k < length; k++) {
                    if (nums[i] + nums[j] + nums[k] == 0) {
                        List<Integer> elem = Arrays.asList(nums[i], nums[j], nums[k]);
                        if (tempSet.contains(elem)) {
                            continue;
                        }
                        tempSet.add(elem);
                        result.add(elem);
                    }
                }
            }
        }
        return result;
    }
}
