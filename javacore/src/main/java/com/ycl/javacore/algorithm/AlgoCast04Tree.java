package com.ycl.javacore.algorithm;

/**
 * User: OF1089 杨成龙
 * Date: 2019/7/31
 * Time: 10:51 PM
 * Desc: 判断某个二叉树，是否是对称
 */
public class AlgoCast04Tree {
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            this.val = x;
        }
    }

    /**
     * 递归比较两个节点以及所有子节点
     *
     * @param left
     * @param right
     * @return
     */
    private boolean isSymmetric(TreeNode left, TreeNode right) {
        if (left != null && right != null) {
            return left.val == right.val
                    && isSymmetric(left.left, right.right)
                    && isSymmetric(left.right, right.left);
        } else {
            return left == null
                    && right == null;
        }
    }

    /**
     * 递归调用节点的左右节点
     *
     * @param root
     * @return
     */
    public boolean isSymmetricTreeRecursive(TreeNode root) {
        if (root == null) {
            return true;
        } else {
            return isSymmetric(root.left, root.right);
        }
    }
}
