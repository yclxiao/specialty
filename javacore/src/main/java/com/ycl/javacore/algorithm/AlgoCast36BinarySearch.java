package com.ycl.javacore.algorithm;

/**
 * User: OF1089 杨成龙
 * Date: 2019/8/14
 * Time: 1:50 PM
 * Desc: 二分查找
 */
public class AlgoCast36BinarySearch {

    /**
     * 二分查找
     *
     * @param nums
     * @param target
     * @return
     */
    public int binarySearch(int[] nums, int target) {
        int low = 0;
        int hight = nums.length;
        int mid;
        while (low <= hight) {
            mid = (low + hight) / 2;
            if (nums[mid] < target) {
                low = mid + 1;
            } else if (nums[mid] > target) {
                hight = mid - 1;
            } else {
                return mid;
            }
        }
        return -1;
    }
}
