package com.ycl.javacore.algorithm;

/**
 * User: OF1089 杨成龙
 * Date: 2019/8/25
 * Time: 8:48 AM
 * Desc: 求平方根
 */
public class AlgoCast54 {
    public int sqrtBinarySearch(int n) {
        int low = 0;
        int high = n;
        while (low <= high) {
            int mid = (low + high) / 2;
            int midPower = mid * mid;
            if (midPower > n) {
                high = mid - 1;
            } else if (mid < n) {
                low = mid + 1;
            } else {
                return mid;
            }
        }

        return high;
    }
}
