package com.ycl.javacore.algorithm;

/**
 * User: 杨成龙
 * Date: 2020/4/7
 * Time: 7:36 上午
 * Desc: 计算一个整数，二进制表示的1的个数
 */
public class AlgoCasts49 {

    public int numOfOneWithMask(int n) {
        int count = 0;
        int mask = 1;

        while (mask != 0) {
            if ((mask & n) != 0) {
                count++;
            }
            mask <<= 1;
            System.out.println(mask);
        }

        return count;
    }

    public static void main(String[] args) {
//        AlgoCasts50 algoCasts50 = new AlgoCasts50();
//        System.out.println(algoCasts50.numOfOneWithMask(12));

        int a = 1;

        for (int i = 0; i < 32; i++) {
            a <<= 1;
            System.out.println(a);
        }
    }

}
