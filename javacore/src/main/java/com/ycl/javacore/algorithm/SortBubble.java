package com.ycl.javacore.algorithm;

import org.junit.Test;

import java.util.Arrays;

/**
 * User: OF1089 杨成龙
 * Date: 2019/8/16
 * Time: 1:35 PM
 * Desc: 冒泡排序
 */
public class SortBubble {
    /**
     * 冒泡排序
     *
     * @param nums
     * @return
     */
    public void sortBubble(int[] nums) {
        int count = nums.length;
        for (int i = 0; i < count - 1; i++) {
            boolean isSorted = true;
            //每一轮遍历都会产生一个最大数，排在末尾，所以此处 count - 1 - i
            for (int j = 0; j < count - 1 - i; j++) {
                int temp = 0;
                if (nums[j] > nums[j + 1]) {
                    temp = nums[j];
                    nums[j] = nums[j + 1];
                    nums[j + 1] = temp;
                    isSorted = false;
                }
            }
            if (isSorted) {
                break;
            }
        }
    }

    @Test
    public void testClient() {
        int[] nums = {9, 11, 8, 3, 5, 1};
        sortBubble(nums);
        System.out.println(Arrays.toString(nums));
    }
}
