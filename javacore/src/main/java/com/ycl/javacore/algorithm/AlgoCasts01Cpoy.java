package com.ycl.javacore.algorithm;

import org.junit.Test;

/**
 * User: OF1089 杨成龙
 * Date: 2019/7/16
 * Time: 9:43 AM
 * Desc: 类描述
 */
public class AlgoCasts01Cpoy {

    private boolean isAlphaNumeric(char c) {
        return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9');
    }

    private boolean isEqualsIgnoreCase(char a, char b) {
        if (a >= 'A' && a <= 'Z') {
            a += 32;
        }
        if (b >= 'A' && b <= 'Z') {
            b += 32;
        }
        return a == b;
    }

    private boolean isPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return true;
        }
        int i = 0;
        int j = s.length() - 1;
        for (; i < j; i++, j--) {
            while (i < j && !isAlphaNumeric(s.charAt(i))) {
                i++;
            }
            while (i < j && !isAlphaNumeric(s.charAt(j))) {
                j--;
            }
            if (i < j && !isEqualsIgnoreCase(s.charAt(i), s.charAt(j))) {
                return false;
            }
        }
        return true;
    }

    @Test
    public void testClient() {
//        String test = " race a E-car ";
        String test = " race a car ";
        System.out.println(isPalindrome(test));
    }

}
