package com.ycl.javacore.algorithm;

/**
 * User: 杨成龙
 * Date: 2020/3/30
 * Time: 7:24 上午
 * Desc: 类描述
 */
public class AlgoMatrixSetZero {

    public void setZeroInMatrix(int[][] a) {
        int m = a.length, n = a[0].length;
        boolean[] rows = new boolean[m];
        boolean[] cols = new boolean[n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (a[i][j] == 0) {
                    rows[i] = true;
                    cols[j] = true;
                }
            }
        }

        for (int i = 0; i < rows.length; i++) {
            for (int j = 0; j < cols.length; j++) {
                if (rows[i] || cols[j]) {
                    a[i][j] = 0;
                }
            }
        }

        //这里只是输出验证
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(a[i][j]);
                if (j == m - 1) {
                    System.out.print("\n");
                }
            }
        }
    }

    public static void main(String[] args) {
        int[][] a = new int[3][3];
        a[0][0] = 1;
        a[0][1] = 2;
        a[0][2] = 3;
        a[1][0] = 4;
        a[1][1] = 0;
        a[1][2] = 6;
        a[2][0] = 0;
        a[2][1] = 8;
        a[2][2] = 9;
        AlgoMatrixSetZero algoMatrixSetZero = new AlgoMatrixSetZero();
        algoMatrixSetZero.setZeroInMatrix(a);
    }
}
