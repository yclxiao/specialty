package com.ycl.javacore.exam.observer;

/**
 * User: OF1089 杨成龙
 * Date: 2019/3/25
 * Time: 9:38 PM
 * Desc: 类描述
 */
public class TwoObserver implements Observer {
    @Override
    public void doAction() {
        System.out.println("我是第二个观察者，观察到变化之后，准备发起相应策略");
    }
}
