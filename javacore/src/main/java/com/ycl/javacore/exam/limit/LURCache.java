package com.ycl.javacore.exam.limit;

import java.util.LinkedHashMap;

/**
 * User: OF1089 杨成龙
 * Date: 2019/3/27
 * Time: 9:48 AM
 * Desc: 类描述
 */
public class LURCache<K, V> {
    private static int capacity = 3;
    private LinkedHashMap<K, V> cacheMap = new LinkedHashMap<K, V>();

    public void put(K k,V v) {
        if (cacheMap.size() >= capacity) {
            cacheMap.remove(k);
        }
    }
}
