package com.ycl.javacore.exam.limit;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * User: OF1089 杨成龙
 * Date: 2019/3/26
 * Time: 1:47 PM
 * Desc: 类描述
 */
public class AtomicIntegerTwo {
    private static AtomicInteger counter = new AtomicInteger(0);

    public static void bizMethod() throws InterruptedException {
        int value = counter.incrementAndGet();
        System.out.println(value);
        if (value > 100) {
            System.out.println(Thread.currentThread().getName() + "被拒绝");
            return;
        }

        System.out.println(Thread.currentThread().getName() + "执行业务逻辑");
        Thread.sleep(1000);
        counter.decrementAndGet();
    }

    public static void main(String[] args) {
        for (int i = 0; i < 1000; i++) {
            new Thread(() -> {
                try {
                    AtomicIntegerTwo.bizMethod();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }
}
