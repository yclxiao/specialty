package com.ycl.javacore.exam.observer;

/**
 * User: OF1089 杨成龙
 * Date: 2019/3/25
 * Time: 9:31 PM
 * Desc: 观察者
 */
public interface Observer {
    /**
     * 观察到之后做出反应
     */
    void doAction();
}
