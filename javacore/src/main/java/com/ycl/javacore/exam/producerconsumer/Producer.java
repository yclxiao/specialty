package com.ycl.javacore.exam.producerconsumer;

/**
 * User: OF1089 杨成龙
 * Date: 2019/3/25
 * Time: 9:46 PM
 * Desc: 类描述
 */
public class Producer implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 3000; i++) {
            Basket.instance.addWoWo("wowo");
            try {
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
