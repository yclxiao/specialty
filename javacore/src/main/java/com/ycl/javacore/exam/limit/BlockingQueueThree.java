package com.ycl.javacore.exam.limit;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * User: OF1089 杨成龙
 * Date: 2019/3/26
 * Time: 1:56 PM
 * Desc: 类描述
 */
public class BlockingQueueThree {
    private static BlockingQueue<Integer> reqQueue = new ArrayBlockingQueue<Integer>(100);

    public static void bizMethod() throws InterruptedException {
        if (!reqQueue.offer(1)) {
            System.out.println(Thread.currentThread().getName() + "被拒绝");
            return;
        }

        System.out.println(Thread.currentThread().getName() + "执行业务逻辑");
        Thread.sleep(1000);//模拟处理业务逻辑需要1秒

        reqQueue.poll();
    }

    public static void main(String[] args) {
        for (int i = 0; i < 1000; i++) {
            new Thread(() -> {
                try {
                    BlockingQueueThree.bizMethod();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }
}
