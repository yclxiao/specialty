package com.ycl.javacore.exam.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * User: OF1089 杨成龙
 * Date: 2019/3/25
 * Time: 9:38 PM
 * Desc: 发起调用
 */
public class Client {
    public static void main(String[] args) {
        Observable observable = new Observable();
        Observer one = new OneObserver();
        Observer two = new TwoObserver();

        List<Observer> observableList = new ArrayList<>();
        observableList.add(one);
        observableList.add(two);
        observable.setObserverList(observableList);

        observable.beginAction();
    }
}
