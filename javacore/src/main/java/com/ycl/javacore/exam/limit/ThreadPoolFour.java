package com.ycl.javacore.exam.limit;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * User: OF1089 杨成龙
 * Date: 2019/3/26
 * Time: 2:01 PM
 * Desc: 类描述
 */
public class ThreadPoolFour {
    private static BlockingQueue<Runnable> reqQueue = new ArrayBlockingQueue<Runnable>(100);

    public static void bizMethod() {
        System.out.println(Thread.currentThread().getName() + "正在执行");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        /*ThreadPoolExecutor threadPoolExecutor =
                new ThreadPoolExecutor(10, 20, 1000, TimeUnit.SECONDS, reqQueue, new ThreadPoolExecutor.CallerRunsPolicy());

        for (int i = 0; i < 1000; i++) {
            threadPoolExecutor.submit(() -> {
                ThreadPoolFour.bizMethod();
            });
        }*/

        long time = System.currentTimeMillis();
        System.out.println(time);
        System.out.println(time << 3);
        System.out.println(time | 6);
    }

}
