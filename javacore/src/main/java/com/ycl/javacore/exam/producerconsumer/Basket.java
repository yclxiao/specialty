package com.ycl.javacore.exam.producerconsumer;

import java.util.ArrayList;
import java.util.List;

/**
 * User: OF1089 杨成龙
 * Date: 2019/3/25
 * Time: 9:45 PM
 * Desc: 装馒头的篮子
 */
public class Basket {

    private Basket() {

    }

    public static Basket instance = new Basket();

    private int capacity = 10;
    private List<String> wowoList = new ArrayList<>();

    public synchronized void addWoWo(String wowo) {
        while (wowoList.size() >= capacity) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        wowoList.add(wowo);
        System.out.println("生产线程：" + Thread.currentThread().getName() + "--- size: " + wowoList.size());
        notifyAll();
    }

    public synchronized void takeWowo() {
        while (wowoList.size() == 0) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        wowoList.remove(0);
        System.out.println("消费线程：" + Thread.currentThread().getName() + "--- size: " + wowoList.size());
        notifyAll();
    }
}
