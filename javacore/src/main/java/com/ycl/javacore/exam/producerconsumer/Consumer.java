package com.ycl.javacore.exam.producerconsumer;

/**
 * User: OF1089 杨成龙
 * Date: 2019/3/25
 * Time: 9:59 PM
 * Desc: 类描述
 */
public class Consumer implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 3000; i++) {
            Basket.instance.takeWowo();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
