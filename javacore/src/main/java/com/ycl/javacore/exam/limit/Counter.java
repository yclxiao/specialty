package com.ycl.javacore.exam.limit;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

public class Counter {

    static HashMap<Long, AtomicLong> count = new HashMap<>();

    static long limits  = 10;
    static int  counter = 0;

    public static synchronized int getCounter() throws Exception {
        while (true) {
            //获取当前的时间戳作为key
            Long currentSeconds = System.currentTimeMillis() / 1000;
            AtomicLong atomicLong = count.get(currentSeconds) == null ? new AtomicLong(0) : count.get(currentSeconds);
            if (atomicLong.getAndIncrement() > limits) {
                continue;
            }
            return counter++;
        }
    }
}