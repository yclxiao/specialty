package com.ycl.javacore.exam.treenode;

/**
 * User: OF1089 杨成龙
 * Date: 2019/3/26
 * Time: 5:04 PM
 * Desc: 类描述
 */
public class TreeNode {
   private TreeNode leftTreeNode;

   private TreeNode rightTreeNode;

   private String value;

    public TreeNode(String value) {
        this.value = value;
    }

    public TreeNode getLeftTreeNode() {
        return leftTreeNode;
    }

    public void setLeftTreeNode(TreeNode leftTreeNode) {
        this.leftTreeNode = leftTreeNode;
    }

    public TreeNode getRightTreeNode() {
        return rightTreeNode;
    }

    public void setRightTreeNode(TreeNode rightTreeNode) {
        this.rightTreeNode = rightTreeNode;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
