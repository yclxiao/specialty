package com.ycl.javacore.exam.observer;

import java.util.List;

/**
 * User: OF1089 杨成龙
 * Date: 2019/3/25
 * Time: 9:32 PM
 * Desc: 被观察者
 */
public class Observable {

    private List<Observer> observerList;

    public void setObserverList(List<Observer> observerList) {
        this.observerList = observerList;
    }

    /**
     * 被观察者准备发出一些动作
     */
    public void beginAction() {
        System.out.println("我准备做些动作啦");
        observerList.forEach(observer -> {
            observer.doAction();
        });
    }
}
