package com.ycl.javacore.exam.sort;

/**
 * User: OF1089 杨成龙
 * Date: 2019/3/25
 * Time: 9:13 PM
 * Desc: 类描述
 */
public class BubbleSortTest {
    public static void sort(int[] data) {
        for (int i = 0; i < data.length - 1; i++) {
            for (int j = 0; j < data.length - i - 1; j++) {
                if (data[j] > data[j + 1]) {
                    int temp = data[j];
                    data[j] = data[j + 1];
                    data[j + 1] = temp;
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] source = new int[]{5, 7, 3, 2, 4};
        sort(source);
        for (int i = 0; i < source.length; i++) {
            System.out.println(source[i]);
        }
    }
}
