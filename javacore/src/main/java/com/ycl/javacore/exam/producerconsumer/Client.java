package com.ycl.javacore.exam.producerconsumer;

/**
 * User: OF1089 杨成龙
 * Date: 2019/3/25
 * Time: 10:00 PM
 * Desc: 类描述
 */
public class Client {
    public static void main(String[] args) {
        Thread producer1 = new Thread(new Producer());
        Thread producer2 = new Thread(new Producer());
        Thread consumer1 = new Thread(new Consumer());
        Thread consumer2 = new Thread(new Consumer());
        Thread consumer3 = new Thread(new Consumer());

        producer1.start();
        producer2.start();

        consumer1.start();
        consumer2.start();
        consumer3.start();
    }
}
