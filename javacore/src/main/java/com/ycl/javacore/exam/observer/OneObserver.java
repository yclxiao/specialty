package com.ycl.javacore.exam.observer;

/**
 * User: OF1089 杨成龙
 * Date: 2019/3/25
 * Time: 9:37 PM
 * Desc: 类描述
 */
public class OneObserver implements Observer {
    @Override
    public void doAction() {
        System.out.println("我是第一个观察者，观察到他的变化了，准备发起相应策略");
    }
}
