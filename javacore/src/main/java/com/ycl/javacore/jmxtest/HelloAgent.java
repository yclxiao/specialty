package com.ycl.javacore.jmxtest;

import javax.management.*;
import java.lang.management.ManagementFactory;

/**
 * User: OF1089 杨成龙
 * Date: 2020/2/19
 * Time: 9:17 上午
 * Desc: 类描述
 */
public class HelloAgent {
    public static void main(String[] args) {
        MBeanServer server = ManagementFactory.getPlatformMBeanServer();
        try {
            ObjectName helloName = new ObjectName("jmxBean:name=hello");
            server.registerMBean(new Hello(), helloName);
            Thread.sleep(60 * 60 * 1000);
        } catch (MalformedObjectNameException e) {
            e.printStackTrace();
        } catch (InstanceAlreadyExistsException e) {
            e.printStackTrace();
        } catch (MBeanRegistrationException e) {
            e.printStackTrace();
        } catch (NotCompliantMBeanException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
