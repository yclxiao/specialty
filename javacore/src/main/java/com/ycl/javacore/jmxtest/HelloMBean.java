package com.ycl.javacore.jmxtest;

/**
 * User: OF1089 杨成龙
 * Date: 2020/2/19
 * Time: 9:15 上午
 * Desc: 类描述
 */
public interface HelloMBean {
    public String getName();

    public void setName(String name);

    public String getAge();

    public void setAge(String age);

    public void helloWorld();

    public void helloWorld(String str);

    public void getTelephone();
}
