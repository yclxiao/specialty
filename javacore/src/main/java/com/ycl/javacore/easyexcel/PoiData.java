package com.ycl.javacore.easyexcel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import org.springframework.util.StringUtils;

import java.util.stream.Stream;

/**
 * User: 杨成龙
 * Date: 2020/4/23
 * Time: 10:02 上午
 * Desc: 类描述
 */
@Data
public class PoiData {
    /*poi.getString("id");
            poi.getString("name");
            poi.getString("tel");
            poi.getString("cityname");
            poi.getString("adname");
            poi.getString("address");
            poi.getString("type");*/

    @ExcelProperty("高德ID")
    private String id;

    @ExcelProperty("名称")
    private String name;

    @ExcelProperty("电话")
    private String tel;

    @ExcelProperty("省")
    private String pname;

    @ExcelProperty("城市")
    private String cityname;

    @ExcelProperty("区")
    private String adname;

    @ExcelProperty("地址")
    private String address;

    @ExcelProperty("经度")
    private String longitude;

    @ExcelProperty("纬度")
    private String latitude;

    @ExcelProperty("高德分类")
    private String type;
}
