package com.ycl.javacore.core.interfaceabstract;

import java.util.Random;

/**
 * User: OF1089 杨成龙
 * Date: 2019/9/2
 * Time: 6:34 PM
 * Desc: 类描述
 */
public abstract class AbstractTest implements TestInterface {
    @Override
    public void method1() {
        System.out.println("method1");
    }

    public static void main(String[] args) {
        Random ran = new Random();
        int a = ran.nextInt(99999999);
        int b = ran.nextInt(99999999);
        long l = a * 10000000L + b;
        String num = String.valueOf(l);
        System.out.println(num);

        System.out.println(System.currentTimeMillis());
    }
}
