package com.ycl.javacore.core.interfaceabstract;

/**
 * User: OF1089 杨成龙
 * Date: 2019/9/2
 * Time: 6:34 PM
 * Desc: 类描述
 */
public interface TestInterface {
    void method1();

    void method2();
}
