package com.ycl.javacore.core.generics;

import lombok.Data;

/**
 * User: OF1089 杨成龙
 * Date: 2019/9/9
 * Time: 10:11 AM
 * Desc: 类描述
 */
@Data
public class UserDTO {
    private String name;
    private Integer age;
}
