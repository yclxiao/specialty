package com.ycl.javacore.core.generics;

/**
 * User: OF1089 杨成龙
 * Date: 2019/9/9
 * Time: 10:13 AM
 * Desc: 类描述
 */
public interface DTOConvert<S, T> {
    T convert(S s);
}
