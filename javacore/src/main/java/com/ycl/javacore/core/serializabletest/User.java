package com.ycl.javacore.core.serializabletest;

import java.io.Serializable;

/**
 * User: OF1089 杨成龙
 * Date: 2020/3/13
 * Time: 9:41 上午
 * Desc: 静态变量：加 static 关键字。  瞬态变量：加 transient 关键字。
 */
public class User implements Serializable {

    private static final long serialVersionUID = 1604260497237820982L;

    private String username;
    private String passwd;
    private String age;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
