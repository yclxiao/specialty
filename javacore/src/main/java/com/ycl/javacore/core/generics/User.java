package com.ycl.javacore.core.generics;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.BeanUtils;

/**
 * User: OF1089 杨成龙
 * Date: 2019/9/9
 * Time: 10:11 AM
 * Desc: 类描述
 */
@Data
@AllArgsConstructor
public class User {
    private String name;
    private Integer age;

    public UserDTO convert2UserDTO() {
        DTOConvert<User,UserDTO> dtoConvert = new UserDTOConvert();
        UserDTO userDTO = dtoConvert.convert(this);
        return userDTO;
    }

    private class UserDTOConvert implements DTOConvert<User,UserDTO> {

        @Override
        public UserDTO convert(User user) {
            UserDTO userDTO = new UserDTO();
            BeanUtils.copyProperties(user, userDTO);
            return userDTO;
        }
    }

}
