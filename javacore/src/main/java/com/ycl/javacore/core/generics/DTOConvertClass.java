package com.ycl.javacore.core.generics;

/**
 * User: OF1089 杨成龙
 * Date: 2019/9/9
 * Time: 10:13 AM
 * Desc: 类描述
 * 如果是泛型类，则在方法前面无需添加  <S>
 * 如果不是泛型方法，只是有一个泛型方法，则需要添加 <S>
 */
public class DTOConvertClass {
    public <S> S convert(S s) {
        return null;
    }
}
