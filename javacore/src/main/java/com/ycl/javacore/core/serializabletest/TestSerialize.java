package com.ycl.javacore.core.serializabletest;

import java.io.*;

/**
 * User: OF1089 杨成龙
 * Date: 2020/3/13
 * Time: 9:42 上午
 * Desc: 类描述
 */
public class TestSerialize {
    public static void main(String[] args) {
        /*User user = new User();
        user.setUsername("hengheng");
        user.setPasswd("123456");

        System.out.println("read before Serializable");
        System.out.println("username:" + user.getUsername());
        System.out.println("password:" + user.getPasswd());

        try {
            ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("/Users/yclxiao/Project/java/specialty/javacore/src/main/java/com/ycl/javacore/core/serializabletest/user.txt"));
            os.writeObject(user);
            os.flush();
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        User user;
        try {
            ObjectInputStream is = new ObjectInputStream(new FileInputStream("/Users/yclxiao/Project/java/specialty/javacore/src/main/java/com/ycl/javacore/core/serializabletest/user.txt"));
            user = (User) is.readObject();
            is.close();

            System.out.println("read after serializable");
            System.out.println("username:" + user.getUsername());
            System.out.println("password:" + user.getPasswd());

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }



    }
}
