package com.ycl.javacore.core.annotationtest;

import java.lang.annotation.*;

/**
 * User: OF1089 杨成龙
 * Date: 2019/10/7
 * Time: 11:05 AM
 * Desc: 类描述
 */
@Target({ElementType.METHOD})
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface SelfMapping {

    int value() default 0;

    String name() default "";
}
