package com.ycl.javacore.core.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;

/**
 * User: OF1089 杨成龙
 * Date: 2020/2/18
 * Time: 9:43 下午
 * Desc: 类描述
 */
public class FileTest {
    public static void main(String[] args) {
        /*File file = new File("/Users/yclxiao/Desktop/风声收入模型.xlsx");
        File to = new File("/Users/yclxiao/Desktop/result.xlsx");
        try {
            FileChannel fileChannel = new FileInputStream(file.getAbsolutePath()).getChannel();
            FileChannel toChannel = new FileOutputStream(to.getAbsolutePath()).getChannel();
            fileChannel.transferTo(0, file.length(), toChannel);

        } catch (IOException e) {
            e.printStackTrace();
        }*/

        try {
            File file = new File("/Users/yclxiao/Desktop/test.txt");
            File to = new File("/Users/yclxiao/Desktop/aaa.txt");
            FileChannel fileChannel = new FileInputStream(file.getAbsolutePath()).getChannel();
            FileChannel toChannel = new FileOutputStream(to.getAbsolutePath()).getChannel();
            MappedByteBuffer mbb = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, file.length());

            toChannel.write(mbb);
            mbb.clear();

            Charset charset = Charset.forName("UTF-8");
            CharsetDecoder charsetDecoder = charset.newDecoder();
            CharBuffer decode = charsetDecoder.decode(mbb);
            System.out.println(decode);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
