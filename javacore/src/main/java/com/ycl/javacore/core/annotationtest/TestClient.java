package com.ycl.javacore.core.annotationtest;

import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * User: OF1089 杨成龙
 * Date: 2019/10/7
 * Time: 11:11 AM
 * Desc: 类描述
 */
public class TestClient {
    public static void main(String[] args) {
        getRequestMappingMethod("com.ycl.javacore.core.annotationtest");
    }

    public static void getRequestMappingMethod(String packagePath) {
        //设置扫描路径
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setUrls(ClasspathHelper.forPackage(packagePath))
                .setScanners(new MethodAnnotationsScanner()));

        Set<Method> methodSet = reflections.getMethodsAnnotatedWith(SelfMapping.class);

        Map<String, String> methodAnnotation = new HashMap<>();

        methodSet.forEach(method -> {
            SelfMapping selfMapping = method.getAnnotation(SelfMapping.class);
            if (selfMapping != null) {
                int value = selfMapping.value();
                String name = selfMapping.name();

                methodAnnotation.put(method.getName(), name.concat("-").concat(String.valueOf(value)));
            }
        });

        Set<Map.Entry<String, String>> entrySet = methodAnnotation.entrySet();
        for (Map.Entry<String, String> entry : entrySet) {
            System.out.println(entry.getValue());
        }
    }
}
