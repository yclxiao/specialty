package com.ycl.javacore.core.annotationtest;

/**
 * User: OF1089 杨成龙
 * Date: 2019/10/7
 * Time: 11:09 AM
 * Desc: 类描述
 */
public class UserController {
    @SelfMapping(value = 10, name = "testMethod1")
    public void testMethod1() {
        System.out.println("testMethod1");
    }

    @SelfMapping(value = 20, name = "testMethod2")
    public void testMethod2() {
        System.out.println("testMethod2");
    }

    public void testMethod3() {
        System.out.println("testMethod3");
    }
}
