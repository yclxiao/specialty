package com.ycl.javacore.core.generics;

/**
 * User: OF1089 杨成龙
 * Date: 2019/9/9
 * Time: 10:25 AM
 * Desc: 类描述
 */
public class Client {
    public static void main(String[] args) {
        User user = new User("ycl", 30);
        UserDTO userDTO = user.convert2UserDTO();
        System.out.println(userDTO.getName());
    }
}
