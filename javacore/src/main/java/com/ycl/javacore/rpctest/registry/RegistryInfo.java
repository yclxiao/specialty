package com.ycl.javacore.rpctest.registry;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * User: OF1089 杨成龙
 * Date: 2019/8/23
 * Time: 3:44 PM
 * Desc: 类描述
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class RegistryInfo {
    private String hostname;
    private String ip;
    private Integer port;
}
