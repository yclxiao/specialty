package com.ycl.javacore.rpctest.rpc.protocol;

import com.alibaba.fastjson.JSONObject;
import com.ycl.javacore.rpctest.cluster.LoadBalancer;
import com.ycl.javacore.rpctest.cluster.RandomLoadbalancer;
import com.ycl.javacore.rpctest.common.InvokeUtils;
import com.ycl.javacore.rpctest.config.ReferenceConfig;
import com.ycl.javacore.rpctest.config.ServiceConfig;
import com.ycl.javacore.rpctest.remote.NettyClient;
import com.ycl.javacore.rpctest.remote.NettyServer;
import com.ycl.javacore.rpctest.remote.RpcResponse;
import com.ycl.javacore.rpctest.registry.Registry;
import com.ycl.javacore.rpctest.registry.RegistryInfo;
import com.ycl.javacore.rpctest.registry.ZookeeperRegistry;
import com.ycl.javacore.rpctest.rpc.DefaultInvoker;
import com.ycl.javacore.rpctest.rpc.Invoker;
import io.netty.channel.ChannelHandlerContext;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.LongAdder;

/**
 * User: OF1089 杨成龙
 * Date: 2019/8/24
 * Time: 8:09 AM
 * Desc: 服务导出 服务引用
 */
public class ProxyProtocol {
    private List<ServiceConfig> serviceConfigs;
    private List<ReferenceConfig> referenceConfigs;
    private int port;
    private Registry registry;
    private Map interfaceMethods = new ConcurrentHashMap();
    private Map<Class, List<RegistryInfo>> interfacesMethodRegistryList = new ConcurrentHashMap<>();

    private Map<RegistryInfo, ChannelHandlerContext> channels = new ConcurrentHashMap<>();
    private ConcurrentLinkedQueue<RpcResponse> responses = new ConcurrentLinkedQueue<>();

    private LoadBalancer loadBalancer = new RandomLoadbalancer();
    private Map<String, Invoker> inProgressInvoker = new ConcurrentHashMap<>();
    private ResponseProcessor[] processors;

    private NettyServer nettyServer;

    /**
     * 负责生成requestId的类
     */
    private LongAdder requestIdWorker = new LongAdder();

    public ProxyProtocol(String registryUrl, List<ServiceConfig> serviceConfigs,
                         List<ReferenceConfig> referenceConfigs, int port) throws Exception {
        this.serviceConfigs = serviceConfigs == null ? new ArrayList<>() : serviceConfigs;
        this.referenceConfigs = referenceConfigs == null ? new ArrayList<>() : referenceConfigs;
        this.port = port;

        //实例化注册中心
        initRegistry(registryUrl);

        //将接口注册到注册中心，从注册中心获取接口，初始化服务接口列表
        InetAddress addr = InetAddress.getLocalHost();
        String hostName = addr.getHostName();
        String hostAddress = addr.getHostAddress();
        RegistryInfo registryInfo = new RegistryInfo(hostName, hostAddress, this.port);
        doRegistry(registryInfo);

        if (!this.serviceConfigs.isEmpty()) {
            nettyServer = new NettyServer(this.serviceConfigs, interfaceMethods);
            nettyServer.init(port);
        }

        if (!this.referenceConfigs.isEmpty()) {
            // step 5：启动处理响应的processor
            initProcessor();
        }
    }

    private void doRegistry(RegistryInfo registryInfo) throws Exception {
        for (ServiceConfig config : serviceConfigs) {
            Class type = config.getType();
            registry.register(type, registryInfo);
            Method[] declaredMethods = type.getDeclaredMethods();
            for (Method method : declaredMethods) {
                String identify = InvokeUtils.buildInterfaceMethodIdentify(type, method);
                interfaceMethods.put(identify, method);
            }
        }

        for (ReferenceConfig referenceConfig : referenceConfigs) {
            List<RegistryInfo> registryInfoList = registry.fetchRegistry(referenceConfig.getType());
            if (registryInfoList != null) {
                interfacesMethodRegistryList.put(referenceConfig.getType(), registryInfoList);
                initChannel(registryInfoList);
            }
        }
    }

    private void initChannel(List<RegistryInfo> registryInfoList) throws InterruptedException {
        for (RegistryInfo registryInfo : registryInfoList) {
            if (!channels.containsKey(registryInfo)) {
                System.out.println("开始建立连接：" + registryInfo.getIp() + ", " + registryInfo.getPort());
                NettyClient nettyClient = new NettyClient(registryInfo.getIp(), registryInfo.getPort());
                nettyClient.setMessageCallback(message -> {
                    // 这里收单服务端返回的消息，先压入队列
                    RpcResponse response = JSONObject.parseObject(message, RpcResponse.class);
                    responses.offer(response);
                    synchronized (ProxyProtocol.this) {
                        ProxyProtocol.this.notifyAll();
                    }
                });
                ChannelHandlerContext ctx = nettyClient.getCtx();
                channels.put(registryInfo, ctx);
            }
        }
    }

    private void initRegistry(String registryUrl) {
        if (registryUrl.startsWith("zookeeper://")) {
            registryUrl = registryUrl.substring(12);
            registry = new ZookeeperRegistry(registryUrl);
        } else if (registryUrl.startsWith("multicast://")) {

        }
    }

    /**
     * 获取调用服务
     */
    @SuppressWarnings("unchecked")
    public <T> T getService(Class clazz) {
        return (T) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{clazz}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                String methodName = method.getName();
                if ("equals".equals(methodName) || "hashCode".equals(methodName)) {
                    throw new IllegalAccessException("不能访问" + methodName + "方法");
                }
                if ("toString".equals(methodName)) {
                    return clazz.getName() + "#" + methodName;
                }


                // step 1: 获取服务地址列表
                List registryInfoList = interfacesMethodRegistryList.get(clazz);

                if (registryInfoList == null) {
                    throw new RuntimeException("无法找到服务提供者");
                }

                // step 2： 负载均衡
                RegistryInfo registryInfo = loadBalancer.choose(registryInfoList);


                ChannelHandlerContext ctx = channels.get(registryInfo);
                String identify = InvokeUtils.buildInterfaceMethodIdentify(clazz, method);
                String requestId;
                synchronized (ProxyProtocol.this) {
                    requestIdWorker.increment();
                    requestId = String.valueOf(requestIdWorker.longValue());
                }
                Invoker invoker = new DefaultInvoker(method.getReturnType(), ctx, requestId, identify);
                inProgressInvoker.put(identify + "#" + requestId, invoker);
                return invoker.invoke(args);
            }
        });
    }

    private void initProcessor() {
        // 事实上，这里可以通过配置文件读取，启动多少个processor
        int num = 3;
        processors = new ResponseProcessor[num];
        for (int i = 0; i < 3; i++) {
            processors[i] = createProcessor(i);
            processors[i].start();
        }
    }

    private ResponseProcessor createProcessor(int i) {
        ResponseProcessor processor = new ResponseProcessor("dealThread-" + i);
        return processor;
    }

    /**
     * 处理响应的线程
     */
    private class ResponseProcessor extends Thread {
        public ResponseProcessor(String s) {
            super(s);
        }

        @Override
        public void run() {
            System.out.println("启动响应处理线程：" + getName());
            while (true) {
                // 多个线程在这里获取响应，只有一个成功
                RpcResponse response = responses.poll();
                if (response == null) {
                    try {
                        synchronized (ProxyProtocol.this) {
                            // 如果没有响应，先休眠
                            ProxyProtocol.this.wait();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    System.out.println("收到一个响应：" + response);
                    String interfaceMethodIdentify = response.getInterfaceMethodIdentify();
                    String requestId = response.getRequestId();
                    String key = interfaceMethodIdentify + "#" + requestId;
                    Invoker invoker = inProgressInvoker.remove(key);
                    invoker.setResult(response.getResult());
                }
            }
        }
    }
}
