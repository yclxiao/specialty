package com.ycl.javacore.rpctest.demo;

/**
 * User: OF1089 杨成龙
 * Date: 2019/8/23
 * Time: 3:27 PM
 * Desc: 类描述
 */
public interface HelloService {
    String hello(TestBean testBean);
}
