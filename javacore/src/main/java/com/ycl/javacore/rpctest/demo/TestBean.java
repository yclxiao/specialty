package com.ycl.javacore.rpctest.demo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * User: OF1089 杨成龙
 * Date: 2019/8/23
 * Time: 3:28 PM
 * Desc: 类描述
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TestBean {
    private String name;
    private Integer age;
}
