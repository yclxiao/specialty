package com.ycl.javacore.rpctest.remote;

import lombok.Data;

/**
 * User: OF1089 杨成龙
 * Date: 2019/8/23
 * Time: 5:47 PM
 * Desc: 类描述
 */
@Data
public class RpcResponse {
    private String result;
    private String interfaceMethodIdentify;
    private String requestId;

    public static RpcResponse create(String result, String interfaceMethodIdentify, String requestId) {
        /*{"interfaceMethodIdentify":"interface=com.study.rpc.test.producer.HelloService&method=sayHello&
            parameter=com.study.rpc.test.producer.TestBean","requestId":"3",
            "result":"\"牛逼,我收到了消息：TestBean{name='张三', age=20}\""}*/

        RpcResponse response = new RpcResponse();
        response.setRequestId(requestId);
        response.setResult(result);
        response.setInterfaceMethodIdentify(interfaceMethodIdentify);
        return response;
    }
}
