package com.ycl.javacore.rpctest.demo;

import com.ycl.javacore.rpctest.rpc.protocol.ProxyProtocol;
import com.ycl.javacore.rpctest.config.ReferenceConfig;

import java.util.Collections;

/**
 * User: OF1089 杨成龙
 * Date: 2019/8/23
 * Time: 9:39 PM
 * Desc: 类描述
 */
public class TestConsumer {
    public static void main(String[] args) throws Exception {
        String connectionString = "zookeeper://localhost:2181";
        ReferenceConfig config = new ReferenceConfig(HelloService.class);
        ProxyProtocol ctx = new ProxyProtocol(connectionString, null, Collections.singletonList(config),
                50071);
        HelloService helloService = ctx.getService(HelloService.class);
        System.out.println("sayHello(TestBean)结果为：" + helloService.hello(new TestBean("张三", 20)));
        System.out.println("调用结束");
    }
}
