package com.ycl.javacore.rpctest.cluster;

import com.ycl.javacore.rpctest.registry.RegistryInfo;

import java.util.List;

public interface LoadBalancer {

    /**
     * 选择一个生产者
     *
     * @param registryInfoList 生产者列表
     * @return 选中的生产者
     */
    RegistryInfo choose(List<RegistryInfo> registryInfoList);

}