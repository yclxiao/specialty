package com.ycl.javacore.rpctest.demo;

import com.ycl.javacore.rpctest.config.ServiceConfig;
import com.ycl.javacore.rpctest.rpc.protocol.ProxyProtocol;

import java.util.ArrayList;
import java.util.List;

/**
 * User: OF1089 杨成龙
 * Date: 2019/8/23
 * Time: 9:32 PM
 * Desc: 类描述
 */
public class TestProducer {
    public static void main(String[] args) throws Exception {
        String registryUrl = "zookeeper://localhost:2181";
        int port = 50071;

        HelloService service = new HelloServiceImpl();
        ServiceConfig config = new ServiceConfig(HelloService.class, service);
        List<ServiceConfig> serviceConfigList = new ArrayList<>();
        serviceConfigList.add(config);

        ProxyProtocol ctx = new ProxyProtocol(registryUrl, serviceConfigList, null, port);

    }
}
