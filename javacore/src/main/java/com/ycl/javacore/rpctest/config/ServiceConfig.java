package com.ycl.javacore.rpctest.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 * User: OF1089 杨成龙
 * Date: 2019/8/23
 * Time: 3:40 PM
 * Desc: 类描述
 */
@Data
@ToString
@AllArgsConstructor
public class ServiceConfig {
    public Class type;
    public Object instance;
}
