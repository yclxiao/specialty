package com.ycl.javacore.rpctest.rpc;

/**
 * User: OF1089 杨成龙
 * Date: 2019/8/24
 * Time: 9:13 AM
 * Desc: 类描述
 */
public interface Invoker<T> {
    T invoke(Object[] args);

    void setResult(String result);
}
