package com.ycl.javacore.rpctest.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * User: OF1089 杨成龙
 * Date: 2019/8/24
 * Time: 8:08 AM
 * Desc: 类描述
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReferenceConfig {
    private Class type;
}
