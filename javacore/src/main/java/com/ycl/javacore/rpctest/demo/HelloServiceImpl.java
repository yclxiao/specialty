package com.ycl.javacore.rpctest.demo;

/**
 * User: OF1089 杨成龙
 * Date: 2019/8/23
 * Time: 6:05 PM
 * Desc: 类描述
 */
public class HelloServiceImpl implements HelloService {
    @Override
    public String hello(TestBean testBean) {
        return "牛逼，我收到了消息：" + testBean;
    }
}
