package com.ycl.javacore.rpctest.cluster;

import com.ycl.javacore.rpctest.registry.RegistryInfo;

import java.util.List;
import java.util.Random;

public class RandomLoadbalancer implements LoadBalancer {

    @Override
    public RegistryInfo choose(List<RegistryInfo> registryInfoList) {
        Random random = new Random();
        int index = random.nextInt(registryInfoList.size());
        return registryInfoList.get(index);
    }
}