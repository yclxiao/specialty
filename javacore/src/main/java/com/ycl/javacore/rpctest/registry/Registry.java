package com.ycl.javacore.rpctest.registry;

import java.util.List;

/**
 * User: OF1089 杨成龙
 * Date: 2019/8/23
 * Time: 3:43 PM
 * Desc: 类描述
 */
public interface Registry {
    void register(Class clazz, RegistryInfo registryInfo) throws Exception;

    List<RegistryInfo> fetchRegistry(Class clazz) throws Exception;
}
